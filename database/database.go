package database

import (
	"bookstore/model"

	"github.com/stretchr/testify/mock"
)

type DBClient interface {
	Insert(model.Book) error
	FindById(id int) (model.Book, error)
	DeleteById(id int) error
	List() ([]model.Book, error)
}

type MockDBClient struct {
	mock.Mock
}

func (m *MockDBClient) Insert(book model.Book) error {
	args := m.Called(book)
	return args.Error(1)
}

func (m *MockDBClient) FindById(id int) (model.Book, error) {
	args := m.Called(id)
	return args.Get(0).(model.Book), args.Error(1)
}

func (m *MockDBClient) List() ([]model.Book, error) {
	args := m.Called()

	return args.Get(0).([]model.Book), args.Error(1)
}

func (m *MockDBClient) DeleteById(id int) error {
	args := m.Called(id)
	return args.Error(1)
}
