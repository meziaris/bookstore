package database

import (
	"bookstore/global"
	"bookstore/model"
	"errors"
	"fmt"
)

type MemoryClient struct{}

func (r *MemoryClient) Insert(book model.Book) error {
	global.Books = append(global.Books, book)
	return nil
}

func (r *MemoryClient) FindById(id int) (model.Book, error) {
	for i := range global.Books {
		if global.Books[i].ID == id {
			return global.Books[i], nil
		}
	}

	return model.Book{}, errors.New("data not found")
}

func (r *MemoryClient) List() ([]model.Book, error) {
	if len(global.Books) != 0 {
		return global.Books, nil
	}

	return global.Books, errors.New("data not found")
}

func (r *MemoryClient) DeleteById(id int) error {
	for i := range global.Books {
		if global.Books[i].ID == id {
			global.Books = append(global.Books[:i], global.Books[i+1:]...)
			return nil
		}
	}

	return fmt.Errorf("data with id %d not found", id)
}
