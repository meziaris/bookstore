package main

import (
	"bookstore/database"
	"bookstore/model"
	"bookstore/service"
	"fmt"
)

func main() {
	dbClient := database.MemoryClient{}
	booksstore := service.BookService{DBClient: &dbClient}

	fmt.Println("Insert Data")
	dataInsert, err := booksstore.AddBook(model.Book{ID: 1, Title: "The Great Gatsby", Author: "F. Scott Fitzgerald", Quantity: 10, Available: true})
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(dataInsert)
	}

	fmt.Println("------")

	fmt.Println("Find By ID")
	result, err := booksstore.GetBookById(1)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(result)
	}

	fmt.Println("------")

	fmt.Println("Get All Book")
	books, err := booksstore.GetAllBook()
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(books)
	}

	fmt.Println("------")

	fmt.Println("Delete By ID")
	deleted, err := booksstore.RemoveBookById(1)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(deleted)
	}

	fmt.Println("------")

	fmt.Println("Get All Book")
	books, err = booksstore.GetAllBook()
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(books)
	}
}
