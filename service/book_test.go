package service

import (
	"bookstore/database"
	"bookstore/model"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestAddBook_normalFlow(t *testing.T) {
	book := model.Book{
		ID:        1,
		Title:     "Buku Harian",
		Author:    "Tom & Jerry",
		Quantity:  10,
		Available: true,
	}

	dbClientMock := database.MockDBClient{}
	dbClientMock.On("Insert", mock.Anything).Return(book, nil)

	bookService := BookService{DBClient: &dbClientMock}
	addBook, _ := bookService.AddBook(book)
	assert.Equal(t, book, addBook, "must be equal")
}

func TestAddBook_errorFlow(t *testing.T) {
	book := model.Book{}

	dbClientMock := database.MockDBClient{}
	dbClientMock.On("Insert", mock.Anything).Return(book, errors.New("something an error"))

	bookService := BookService{DBClient: &dbClientMock}
	_, err := bookService.AddBook(book)
	assert.Error(t, err)
}

func TestGetBookById_normalFlow(t *testing.T) {
	book := model.Book{
		ID:        1,
		Title:     "Buku Harian",
		Author:    "Tom & Jerry",
		Quantity:  10,
		Available: true,
	}

	dbClientMock := database.MockDBClient{}
	dbClientMock.On("FindById", mock.Anything).Return(book, nil)

	bookService := BookService{DBClient: &dbClientMock}
	bookResp, _ := bookService.GetBookById(1)
	assert.Equal(t, bookResp, book, "must be equal")
}

func TestGetBookById_errorFlow(t *testing.T) {
	book := model.Book{}

	dbClientMock := database.MockDBClient{}
	dbClientMock.On("FindById", mock.Anything).Return(book, errors.New("data not found"))

	bookService := BookService{DBClient: &dbClientMock}
	_, err := bookService.GetBookById(1)
	assert.Error(t, err)
}

func TestDeletById_normalFlow(t *testing.T) {
	msg := "successfully delete data with id: 1"

	dbClientMock := database.MockDBClient{}
	dbClientMock.On("DeleteById", mock.Anything).Return(msg, nil)

	bookService := BookService{DBClient: &dbClientMock}
	r, _ := bookService.RemoveBookById(1)
	assert.Equal(t, msg, r, "must be equal")
}

func TestDeletById_errorFlow(t *testing.T) {
	msg := "failed to delete data by id: 1"

	dbClientMock := database.MockDBClient{}
	dbClientMock.On("DeleteById", mock.Anything).Return(msg, errors.New("data with id 1 not found"))

	bookService := BookService{DBClient: &dbClientMock}
	_, err := bookService.RemoveBookById(1)
	assert.Error(t, err)
}
