package service

import (
	"bookstore/database"
	"bookstore/model"
	"fmt"
)

type BookService struct {
	DBClient database.DBClient
}

func (bookService *BookService) AddBook(book model.Book) (model.Book, error) {
	err := bookService.DBClient.Insert(book)

	if err != nil {
		return model.Book{}, err
	}

	return book, nil
}

func (bookService *BookService) GetBookById(id int) (model.Book, error) {
	book, err := bookService.DBClient.FindById(id)

	if err != nil {
		return model.Book{}, err
	}

	return book, nil
}

func (bookService *BookService) GetAllBook() ([]model.Book, error) {
	books, err := bookService.DBClient.List()

	if err != nil {
		return []model.Book{}, err
	}

	return books, nil
}

func (bookService *BookService) RemoveBookById(id int) (string, error) {
	err := bookService.DBClient.DeleteById(id)

	if err != nil {
		return fmt.Sprintf("failed to delete data by id: %d", id), err
	}

	return fmt.Sprintf("successfully delete data with id: %d", id), nil
}
